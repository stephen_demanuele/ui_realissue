**Final code of WPF MVVM project for a USA client.** 

I was responsible for the user interface of a particular screen, which had to 

1) offer drag and drop functionality
2) be as interactive as possible

This project uses Telerik TileView control with 2 different templates, and involved some 35 hours of work.